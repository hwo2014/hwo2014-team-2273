package noobbot;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.util.Collection;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;


import com.google.gson.Gson;
//import com.google.gson.JsonArray;
//import com.google.gson.JsonParser;
import java.util.Iterator;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;

public class Main {
	
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
		String botKey = args[3];
		
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
	private TrackPiece[] track;
	public TrackPiece[] readTrack(JSONArray pieces)
	{
		TrackPiece[] trackPieces=new TrackPiece[pieces.size()];
		for (int i = 0; i < pieces.size(); i++) {  
			JSONObject singlePiece = (JSONObject)pieces.get(i);
			double length, radius, angle;
			length=radius=angle=0;
			boolean hasSwitch=false;
			if(singlePiece.get("length")!=null)
			{	
				length = ((Number)singlePiece.get("length")).doubleValue();
				if(singlePiece.get("switch")!=null)
				{	hasSwitch = (boolean)singlePiece.get("switch");}
				trackPieces[i]=new StraightPiece(length,hasSwitch);
			}
			else{
				if(singlePiece.get("radius")!=null)
					radius = ((Number)singlePiece.get("radius")).doubleValue();
				if(singlePiece.get("angle")!=null)
					angle= ((Number)singlePiece.get("angle")).doubleValue();
				trackPieces[i]=new CurvePiece(radius, angle);	
			}
		}
		return trackPieces;
	
	}
    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            
			if (msgFromServer.msgType.equals("carPositions")) {
                send(new Throttle(0.65));
				JSONParser parser=new JSONParser();
				try{
					JSONObject msg = (JSONObject)parser.parse(line);
					JSONArray data = (JSONArray)msg.get("data");
					boolean ourCarFound=false;
					for(int i=0; i<data.size() && !ourCarFound; i++)
					{
						JSONObject dataEntry=(JSONObject)data.get(i);
						JSONObject id=(JSONObject)dataEntry.get("id");
						String name=(String)id.get("name");
						if(name.equals("Alternative"))
						{
							double angle = ((Number)dataEntry.get("angle")).doubleValue();
							JSONObject piecePosition=(JSONObject)dataEntry.get("piecePosition");
							int pieceIndex=((Number)piecePosition.get("pieceIndex")).intValue();
							double inPieceDistance=((Number)piecePosition.get("inPieceDistance")).doubleValue();
							int lap=((Number)piecePosition.get("lap")).intValue();
							
							System.out.println("angle: "+angle+", pieceIndex: "+pieceIndex+", inPieceDistance: "+inPieceDistance); 
						}
					}
				}
				catch(Exception e)
				{System.out.println("something fucked up with positions" +e.getMessage());}
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
				JSONParser parser=new JSONParser();
				try{
				//Object obj = parser.parse(line);

                JSONObject msg = (JSONObject)parser.parse(line);
				JSONObject data = (JSONObject)msg.get("data");
                JSONObject race = (JSONObject)data.get("race");
                JSONObject trackMsg = (JSONObject)race.get("track");
                JSONArray pieces = (JSONArray)trackMsg.get("pieces");
				
				track=readTrack(pieces);
				for(int i=0; i<track.length; i++)
				{
					track[i].toString();
				}
				
				System.out.println("I readed it! ");//+pieces.toString());
				}
				catch(Exception e)
				{System.out.println("something fucked up gameInit" +e.getMessage());}
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

 class InitMsg
{
	private String msgType;
	private InitData data;
	public InitMsg(String msgType, InitData data)
	{
		this.data=data;
		this.msgType=msgType;
		System.out.println("got here");
	}
}
 class InitData
{
	public final Object race;
	public InitData(Object race)
	{
		this.race=race;
		System.out.println("success");
	}
}

 abstract class TrackPiece
{
	public abstract String toString();
}

 class CurvePiece extends TrackPiece
{
	private double radius,angle;
	public CurvePiece(double radius, double angle)
	{
		this.radius=radius;
		this.angle=angle;
	}
	public String toString()
	{
		return "Curve Piece, radius: "+radius+", angle: "+angle;
	}
}

 class StraightPiece extends TrackPiece
{
	private double length;
	private boolean hasSwitch;
	public StraightPiece(double length, boolean hasSwitch)
	{
		this.length=length;
		this.hasSwitch=hasSwitch;
	}
	public String toString()
	{
		return "Straight Piece, length: "+length+", switch: "+hasSwitch;
	}
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}